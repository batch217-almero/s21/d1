//console.log("Test connection");

// ARRAYS are used to store multiple "!!!RELATED!!!" values in a single variable

// Declare Array: []- Array Literals

let	studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926'];

// Common example of Array
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

console.log(grades);
console.log(computerBrands);

// Alternative way of writing an array
let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
];
console.log(myTasks);


// Array with values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

// Array Length Property
let fullName = "Jamie Noble";
console.log(fullName.length);

console.log(cities.length);

// Delete specific item in an array using -1
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// Another way to delete and array item using --
myTasks.length--;
console.log(myTasks.length);
console.log(myTasks);

// Ading an item in array using incrementation (++)
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
theBeatles.length++;
theBeatles.length++;
console.log(theBeatles);

// Reading from Arrays
// Locate item
console.log(grades[0]);
console.log(computerBrands[3]);

console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[4]);

let currentLaker = lakersLegends[2];
console.log(currentLaker);

// Reassigning array values using Array Index
console.log("Array before re-assignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);

// Accessing the last element of an array using "length"
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);

// Adding item to array using index
let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);


newArr[newArr.length] = "Barett Wallace";
console.log(newArr);

// Looping over an Array
for (let index = 0; index < newArr.length; index ++){
	console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40];
numArr[5]=39;
numArr[6]=50;
numArr[7]=60;

for (let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 == 0){
		console.log(numArr[index] + " is divisible by 5");
	} else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}

console.log(numArr);


// Multidimensional Array

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);
console.log(chessBoard[1][4]); //Displays e4
console.log(chessBoard[4][3]); 

console.table(chessBoard); //Display array in console as a Table